<?php

namespace JustCommunication\YandexBounds\service;

use JustCommunication\YandexBounds\models\BoundInterface;
use JustCommunication\YandexBounds\models\Cities;
use JustCommunication\YandexBounds\models\Regions;

class Yandex {

    public static $config;

    public function __construct($config) {
        self::$config = $config;
    }

    public function runScript() {
    	$begin = time();

        $citiesModel = new Cities(self::$config['table_opt']['table_town'], 'town');
        $regionsModel = new Regions(self::$config['table_opt']['table_region'], 'region');

        $this->getBoundsFromYandex($citiesModel);
        $this->getBoundsFromYandex($regionsModel);

        echo "Готово. Время выполнения: " . (time() - $begin) . " сек";
    }

    private function getBoundsFromYandex(BoundInterface $obj) {

        $chunks = $obj->dbData->initChunksAndOffset($obj->tableName, $obj->type);

        for ($i = $chunks; $i > 0; $i--) {
            $obj->getBounds();
        }
    }

}