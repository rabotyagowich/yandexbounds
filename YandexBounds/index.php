<?php

require __DIR__ . '/../vendor/autoload.php';

use JustCommunication\YandexBounds\service\Yandex;
$config = [
            'mode' => 0, // 0 - Обновить все сущ-е записи (колонка bound), 1 - Заполнить только те, которые пустые
            'last_update' => 30, // При режиме: 0 обновит те записи, с момента последнего обновления которых прошло last_update дней
            'limit' => 100, // По сколько записей брать из БД за раз
            'yandexmap_key' => 'your_yandexmap_key',
            'db'=> [
                'host' => 'localhost',
                'db_name' => 'work',
                'db_user' => 'root',
                'db_password' => '',
                'db_port' => '3306'
            ],
            'table_opt' => [
                //имена таблиц, откуда берем данные
                'table_town' => 's_town',
                'table_region' => 's_region',
                'table_country' => 's_country',
                //из каких колонок берем сводные данные
                'col_town_name' => 'name',
                'col_region_name' => 'name',
                'col_country_name' => 'name',
                'col_town_bounds_last_update' => 'updated_at',
                'col_region_bounds_last_update' => 'updated_at',
                //в какие колонки пишем
                'col_town_bounds' => 'bounds',
                'col_region_bounds' => 'bounds',
                //колонки внешних ключей из таблицы гордов->регион город->страна (Архангельск, Архангельская обл., Россия)
                'col_on_region' => 'id_s_region',
                'col_on_country' => 'id_s_country',
            ]
        ];

$execute = new Yandex($config);
$execute->runScript();
