<?php

namespace JustCommunication\YandexBounds\models;

interface BoundInterface
{
    function getBounds();

    function prepareDataToSend(array $data);

    function writeToBase(array $data);
}