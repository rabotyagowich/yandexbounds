<?php

namespace JustCommunication\YandexBounds\models;

use JustCommunication\YandexBounds\service\Yandex;

abstract class BaseModel
{
    public $dbData;
    public $tableName;
    public $type;
    public $chunks;
    protected $url = 'https://geocode-maps.yandex.ru/1.x/';

    public function __construct(string $tableName, string $type) {
        $this->dbData = new DbData();
        $this->tableName = $tableName;
        $this->type = $type;
    }

    protected function sendDataToYandex(string $geo) {

        $params = array('lang' => 'ru_RU', 'apikey' => $this->getKey(), 'format' => 'json', 'results' => 1);
        $params['geocode'] = $geo;

        $curl = curl_init($this->url . '?' . http_build_query($params));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close($curl);

        $array = json_decode($json, true);

        return $this->handleResult($array);

    }

    protected function getKey() {
        return Yandex::$config['yandexmap_key'];
    }

    protected function handleResult(array $array) {

        if (isset($array['statusCode'])) {
           die('Error: ' . $array['message']);
        }

        $bound =
            $array['response']
            ['GeoObjectCollection']
            ['featureMember'][0]
            ['GeoObject']
            ['boundedBy']
            ['Envelope'];

        $pos =
            $array['response']
            ['GeoObjectCollection']
            ['featureMember'][0]
            ['GeoObject']
            ['Point']
            ['pos'];

        $coords = explode(' ', $pos);
        $low = explode(' ', $bound['lowerCorner']);
        $high = explode(' ', $bound['upperCorner']);

        $result = [
            'coordinates' => [(float)$coords[1], (float)$coords[0]],
            'boundedBy' => [
                [(float)$low[1], (float)$low[0]],
                [(float)$high[1], (float)$high[0]]
            ],
        ];


        return json_encode($result);
    }

}