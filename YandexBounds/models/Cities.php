<?php

namespace JustCommunication\YandexBounds\models;

use JustCommunication\YandexBounds\models\BaseModel;
use JustCommunication\YandexBounds\service\Yandex;
use JustCommunication\YandexBounds\models\BoundInterface;

class Cities extends BaseModel implements BoundInterface
{
    public function getBounds() {
        $data = $this->dbData->getTownsWithRegion();

        if (!empty($data)) {
            while ($row = $data->fetch()) {
                $this->prepareDataToSend($row);
            }
        }
    }

    public function prepareDataToSend(array $data) {

        $str = $data['country'] . ' , ' . $data['region'] . ' , ' . $data['city'];
        $result['id'] = $data['id'];
        $result['bounds'] = $this->sendDataToYandex($str);

        return $this->writeToBase($result);
    }

    public function writeToBase(array $arr) {
        $colName = Yandex::$config['table_opt']['col_town_bounds'];

        return $this->dbData->writeBounds($arr, $this->tableName, $colName);
    }

}