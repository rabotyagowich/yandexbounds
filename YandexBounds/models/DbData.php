<?php

namespace JustCommunication\YandexBounds\models;

use JustCommunication\YandexBounds\components\Db;
use JustCommunication\YandexBounds\service\Yandex;
use PDO;

class DbData
{
    private $db;
    private $conf;
    private $chunks = 0;
    private $currStep = 0;
    private $offset = 0;
    private $limit;
    private $modelQuery = [];

    public function __construct()
    {
       $this->db = Db::getConnection()->connection;
       $this->conf = Yandex::$config['table_opt'];
       $this->createModelCondition();
       $this->limit = Yandex::$config['limit'];
    }

    public function getTownsWithRegion() {

        $query = 'SELECT '
            . $this->conf['table_town'] . '.id, '
            . $this->conf['table_town'] . '.' . $this->conf['col_town_name'] .' AS city, '
            . $this->conf['table_region'] . '.' . $this->conf['col_region_name'] .' AS region, '
            . $this->conf['table_country'] . '.' . $this->conf['col_country_name'] . ' AS country'

            . ' FROM ' . $this->conf['table_town']

            . ' JOIN '. $this->conf['table_region'] . ' ON '
            . $this->conf['table_town'] . '.' . $this->conf['col_on_region'] . ' = '. $this->conf['table_region'] .'.id
            
                JOIN '. $this->conf['table_country'] .' ON '
            . $this->conf['table_town'] . '.' . $this->conf['col_on_country'] . ' = '. $this->conf['table_country'] .'.id'
               
            . $this->modelQuery['town'][Yandex::$config['mode']]
               
            . ' LIMIT ' . $this->limit;

        try {
            $result = $this->db->query($query);
        } catch (PDOException $e) {
            die('ERROR: ' . $e->getMessage());
        }

        $this->renewOffsetAndStep();

        return $result;
    }

    public function getRegions() {

        $query = 'SELECT '
            . $this->conf['table_region'] . '.id, '
            . $this->conf['table_region'] . '.' . $this->conf['col_region_name'] .' AS region, '
            . $this->conf['table_country'] . '.' . $this->conf['col_country_name'] . ' AS country'

            . ' FROM ' . $this->conf['table_region']

            . ' JOIN ' . $this->conf['table_country'] . ' ON '
            .$this->conf['table_region'] . '.' . $this->conf['col_on_country'] . ' = ' . $this->conf['table_country'] .'.id'

            .$this->modelQuery['region'][Yandex::$config['mode']]

            . ' LIMIT ' . $this->limit;

        try {
            $result = $this->db->query($query);
        } catch (PDOException $e) {
            die('ERROR: ' . $e->getMessage());
        }

        return $result;
    }

    public function writeBounds(array $arr, string $tableName, string $colName) {

        $query = 'UPDATE ' . $tableName . ' SET '. $colName .' =:val, updated_at = CURDATE() WHERE id=:id';

        try {
            $result = $this->db->prepare($query);
            $result->bindParam(':id', $arr['id'], PDO::PARAM_INT);
            $result->bindParam(':val', $arr['bounds'], PDO::PARAM_STR);

            $result->execute();
        } catch (PDOException $e) {
            die('ERROR: ' . $e->getMessage());
        }

        return $result;
    }

    private function createModelCondition() {
        $this->modelQuery['town'][0] = ' WHERE ' .$this->conf['table_town']. '.' .$this->conf['col_town_bounds_last_update'].
            ' <= (CURDATE() - INTERVAL ' . Yandex::$config['last_update'] . ' DAY)';

        $this->modelQuery['town'][1] = ' WHERE ( ' .$this->conf['table_town']. '.' .$this->conf['col_town_bounds_last_update']. ' is null)';

        $this->modelQuery['region'][0] = ' WHERE ' .$this->conf['table_region']. '.' .$this->conf['col_region_bounds_last_update'].
            ' <= (CURDATE() - INTERVAL ' . Yandex::$config['last_update'] . ' DAY)';

        $this->modelQuery['region'][1] = ' WHERE ( ' .$this->conf['table_region']. '.' .$this->conf['col_region_bounds_last_update']. ' is null)';
    }

    public function initChunksAndOffset(string $tableName, string $type) {

            $query = "SELECT count(*) FROM " . $tableName . $this->modelQuery[Yandex::$config[$type]['mode']];

            try {
                $result = $this->db->prepare($query);
                $result->execute();
            } catch (PDOException $e) {
                die('ERROR: ' . $e->getMessage());
            }

            $this->chunks = ceil($result->fetchColumn() / $this->limit);

            return $this->chunks;
    }

    private function renewOffsetAndStep() {
        $this->currStep = $this->currStep++;
        $this->offset = $this->limit * $this->currStep;
    }
}