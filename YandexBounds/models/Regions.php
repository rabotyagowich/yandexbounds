<?php

namespace JustCommunication\YandexBounds\models;

use JustCommunication\YandexBounds\models\BaseModel;
use JustCommunication\YandexBounds\service\Yandex;
use JustCommunication\YandexBounds\models\BoundInterface;

class Regions extends BaseModel implements BoundInterface
{
    public function getBounds() {
        $data = $this->dbData->getRegions();

        if (!empty($data)) {
            while ($row = $data->fetch()) {
                $this->prepareDataToSend($row);
            }
        }
    }

    public function prepareDataToSend(array $data) {

        $str = $data['country'] . ' , ' . $data['region'];
        $result['id'] = $data['id'];
        $result['bounds'] = $this->sendDataToYandex($str);

        return $this->writeToBase($result);
    }

    public function writeToBase(array $arr) {
        $colName = Yandex::$config['table_opt']['col_region_bounds'];

        return $this->dbData->writeBounds($arr, $this->tableName, $colName);
    }

}