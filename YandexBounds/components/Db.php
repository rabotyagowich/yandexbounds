<?php

namespace JustCommunication\YandexBounds\components;

use JustCommunication\YandexBounds\service\Yandex;
use PDO;

class Db
{
    private static $instance;
    public $connection;

	public static function getConnection() {
        if (empty(self::$connection)) {
            self::$instance = new self();
        }

        return self::$instance;
	}

    private function __construct() {

        $params = Yandex::$config['db'];

        $opt = array(
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );

        try {

            $link = new PDO("mysql:host={$params['host']};
                                port={$params['db_port']};
                                dbname={$params['db_name']}",
                                $params['db_user'],
                                $params['db_password'], $opt);

        } catch (PDOException $e) {
            die('ERROR: ' . $e->getMessage());
        }

        $link->exec("SET NAMES utf8");

        $this->connection = $link;
    }

    public function __clone() {}

}